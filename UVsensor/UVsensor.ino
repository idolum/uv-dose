#include <Arduino.h>
// working modes
// paramA : set the target value for UVA, set paramB to 0
// paramB : set the target value for UVB, set paramA to 0
// accumulate : poll and measure, accumulate
// start : printonly the start message
enum : byte {accumulate, start, info} mode = start;

// pin configuration : input push buttons
const byte pinButtonMode = 8;
const byte pinButtonTgtplus = 7;
const byte pinButtonTgtmoins = 9;
// using pin 6 for the LVD V0 
const int v0Pin = 6;
// using pin 10 for the backlight
const int backLightPin = 10;

// tracking time for readable increase / decrease of tgt
unsigned long int startTgtSet = 0;
unsigned long int nextTgtstep = 0;
// next increment time step
const unsigned short timeStep = 250;
// next scale time step
const uint32_t changeScale= 2000;



// UV dose storage
float doseA = 0;
float doseB = 0;

// targets of UV doses
// target cannot be higher than 9999
int tgtA = 0;
const int minTgt = 0, maxTgt = 9999;

// measurement time
unsigned long startMeas = 0;
char clock[10];

// measurements for normalization (limited by LCD number of digits)
// max value on may 26th
// uva raw: 10800
// uvb raw: 13400
// uva compensated: 5100
// uvb compensated: 6000
// 
// Normalization example : 1 measurement every 2 seconds (30/minute)
// Exposur time in full sun ~ 4 minutes : 120 measurements
// Total for UVA : 612000
// Total for UVB : 720000
// To be normalized on a scale of 200 (arbitrary)
// 720000 / 3600 = 200
// This normalisation factor should not be changed (or the full exposure calibration needs to be rerun)
const long normUV = 3600L;

// UV sensor polling time tracker
unsigned long measTimeStamp = 0;
// this measurement interval should not be changed - or the exposure calibration needs to be rerun.
const long measInterval = 2000;

// LCD related variables
// lcd screen status
bool LCDstatus = true;
// generic string to print
const char space = ' ';
char values[5] = {space, space, space, space};
char fvalues[5] = {space, space, space, space};

// built-in led: blinking durring acquisition
const int ledPin =  LED_BUILTIN;
int ledState = LOW;  
unsigned long blinkTimeStamp = 0;
const long blinkInterval = 2000;
const int bitInterval = 2000;
const int pinBip = 13;
const int bipDuration = 200;
const int bipHeight = 440;

// -------------------------------
// Base libs
// -------------------------------
//I2C for the sensor
// from : https://www.arduino.cc/en/Reference/Wire
// board : UNO, pins A4 (SDA), A5 (SCL)
#include <Wire.h>

// -------------------------------
// UV sensor VEML6075
// -------------------------------
#include <DFRobot_VEML6075.h>
#define VEML6075_ADDR   0x10
// create object
DFRobot_VEML6075_IIC VEML6075(&Wire, VEML6075_ADDR);  

void startingUVsensor() {
  while(VEML6075.begin() != true) {
    Serial.println("VEML6075 begin failed");
    delay(1000);
  }
  Serial.println("VEML6075 begin succeeded");
}


// https://www.arduino.cc/en/Reference/LiquidCrystal
#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);



void startingLCD(){
  // built in led
  pinMode(ledPin, OUTPUT);
  // lcd display
  pinMode(v0Pin, OUTPUT);
  pinMode(backLightPin, OUTPUT);
  // get it visible
  digitalWrite(v0Pin, LOW);
  digitalWrite(backLightPin, HIGH);
  lcd.begin(16,2);
  lcd.display();
  lcd.clear();
  lcd.print("UV dose meter");
}

// print an integer value on 4 characters
void intLCDprint(int val, char (& values)[5] ) {
  // reset to spaces
  memset(values, space, sizeof(values));
  // make sure of upper boundary
  if (val > maxTgt) val = maxTgt;
  if (val < minTgt) val = minTgt;
  // temporary string
  char buf[5];
  // convert to string
  itoa(val, buf, 10);
  // copy
  strcpy(values + sizeof(values) - strlen(buf) - 1, buf);
}

// print a float with 1 digit, one dot, 2 digits 
void floatLCDprint(float val, char (& fvalues)[5]) {
  memset(fvalues, space, sizeof(fvalues));
  if (val>9.99) val = 9.99;
  if (val<0.01) val = 0.01;
  // convert float to string, 4 characters, 2 digits after .
  dtostrf(val, 4, 2, fvalues);
}

// LCD screen during acquisition
// ................
// A> MMM DDDD TTTT
// B> MMM DDDD TTTT
void LCDacquisition(float measA, float measB) {
  // normalisation
  float norm_measA = measA / (float)normUV;
  float norm_measB = measB / (float)normUV;
  int norm_doseA = (long)doseA / normUV;
  int norm_doseB = (long)doseB / normUV;
  Serial.print("lcd meas A : ");
  Serial.println(norm_measA);
  Serial.print("lcd meas B : ");
  Serial.println(norm_measB);

  // first line
  // print UVA measurement
  floatLCDprint(norm_measA, fvalues);
  lcd.setCursor(2,0);
  lcd.print(fvalues);
  // print UVA dose
  intLCDprint(norm_doseA, values);
  lcd.setCursor(7,0);
  lcd.print(values);

  // second line
  // print UVB measurement
  floatLCDprint(norm_measB, fvalues);
  lcd.setCursor(2,1);
  lcd.print(fvalues);
  // print UVB dose
  intLCDprint(norm_doseB, values);
  lcd.setCursor(7,1);
  lcd.print(values);
}

// static part of mode accumulate
void LCDstaticAccumulate() {
  if (mode == accumulate){
    // first line
    lcd.setCursor(0,0);
    lcd.print("A>");
    // print UVA target
    intLCDprint(tgtA, values);
    lcd.setCursor(12,1);
    lcd.print(values);
     // second line
    lcd.setCursor(0,1);
    lcd.print("B>");
    // print UVB target
//    intLCDprint(tgtB, values);
//    lcd.setCursor(12,1);
//    lcd.print(values); */
    lcd.setCursor(13,0);
    lcd.print("tgt");
  }
}

// info screen for runtime since last reset
void LCDinfo(){
  if (mode == info){
    lcd.setCursor(0,0);
    lcd.print("t:");
    unsigned long toc = millis();
    unsigned long run_s = (toc - startMeas)/1000;
    int h = run_s/3600;
    int s = (run_s%3600)%60;
    int m = (run_s%3600)/60;
    char tmp[10];
    sprintf(tmp,"%02d:%02d:%02d",h,m,s);
    if (clock != tmp) {
      lcd.setCursor(3,0);
      strcpy(clock, tmp);
      lcd.print(clock);
    }
    if (clock == tmp) {
      lcd.setCursor(3,1);
      lcd.print(clock);
    }
  }
}

// -------------------------------
// inputs : buttons configuration
// -------------------------------
// inputs : 2 push buttons, one pot 
// push button 1 : digital pin #8 : 
//    single push : loop over the modes. 
//    double push : switch on/off the LCD, keeping the mode
//    long press  : no effect
// push button 2 : digital pin # 7
//    single push : increase target by 1 in param mode, no effect in other modes
//    double push : reset the accumulation to 0, only in accumulate mode
//    long press  : increase target in param mode
// push button 3 : digital pin # 9
//    single push : decrease target by 1 in param mode, no effect in other modes
//    double push : reset the targets to 0, only in param mode
//    long press  : decrease target in param mode
// library : https://github.com/mathertel/OneButton
#include <OneButton.h>
OneButton btn1 = OneButton(pinButtonMode, HIGH, true);
OneButton btn2 = OneButton(pinButtonTgtplus, HIGH, true);
OneButton btn3 = OneButton(pinButtonTgtmoins, HIGH, true);



// button1 double push: LCD on/off
void doubleClickMode() {
  LCDstatus = !LCDstatus;
  if (LCDstatus) {
    digitalWrite(backLightPin, HIGH);
    // we could use a for loop and
    // analogWrite(backlight, i) to fade in and out 
    // but it's using delay() which is blocking.
    // Will fade if I can find a solution with millis();
    lcd.display();
  } else {
    lcd.noDisplay();
    digitalWrite(backLightPin, LOW);
  }
  Serial.println("Doubled Clicked mode button!");
}

// button1 single push : switch to next mode
void singleClickMode() {
  Serial.print("Button Mode single clicked. Mode was: ");
  Serial.println(mode);
  // clear the LCD for the new print
  lcd.clear();
  switch(mode) {
    case info:
      mode = accumulate;
      LCDstaticAccumulate();
      break;
    case accumulate:
      mode = info;
      LCDinfo();
      break;
    default:
      mode = accumulate;
      LCDstaticAccumulate();
      break;
  }
  Serial.print("Button Mode single clicked. Mode is: ");
  Serial.println(mode);
}

// button2 single click : increase tgt
// need to be in mode paramA or paramB 
void singleClickTgtplus(){
  Serial.print("Increase tgt ");
  if (mode == accumulate) {
    if (tgtA < 9999) tgtA +=1;
    Serial.print(" A: ");
    Serial.println(tgtA);
    LCDstaticAccumulate();
  }  
}

// button3 single click : decrease tgt
// need to be in mode paramA or paramB 
void singleClickTgtmoins(){
  Serial.print("Decrease tgt ");
  if (mode == accumulate) {
    if (tgtA > 0) tgtA -=1;
    Serial.print(" A: ");
    Serial.println(tgtA);
    LCDstaticAccumulate();
  }  
}

// button 2 & 3 long press: increase or decrease
void longPushTgtplus(){
  if (mode == accumulate){
    int incr = 1;
    int now = millis();
    int duration = now - startTgtSet;
    if (now - nextTgtstep > timeStep){
      nextTgtstep = now;
      if (duration > changeScale ) {
        incr = 10;
      }
      if (duration > 2*changeScale) {
       incr = 25;
      }
      if (duration > 3*changeScale) {
       incr = 50;
      }
        if (tgtA + incr < maxTgt) tgtA += incr;
        if (tgtA + incr >= maxTgt) tgtA = maxTgt;
    }
    LCDstaticAccumulate();
  }
}

void longPushTgtmoins(){
  if (mode == accumulate){
    int incr = 1;
    int now = millis();
    int duration = now - startTgtSet;
    if (now - nextTgtstep > timeStep){
      nextTgtstep = now;
      if (duration > changeScale ) {
        incr = 10;
      }
      if (duration > 2*changeScale) {
       incr = 25;
      }
      if (duration > 3*changeScale) {
       incr = 50;
      }
        if (tgtA - incr > minTgt) tgtA -= incr;
        if (tgtA - incr <= minTgt) tgtA = 0;
    }
    LCDstaticAccumulate();
  }
}

// start the timer for the increase/decrease of tgt
void triggerTimer(){
  startTgtSet = millis();
  nextTgtstep = startTgtSet;
}
// -------------------------------
// inputs : end of buttons 
// -------------------------------

// bar graph : https://playground.arduino.cc/Code/LcdBarGraph/
//#include <LcdBarGraph.h>
// -- creating bargraph instance, format is (&lcd, lcdNumCols, start X, start Y). So (&lcd, 16, 0, 1) 
// would set the bargraph length to 16 columns and start the bargraph at column 0 on row 1.
//LcdBarGraph  lbga(&lcd, 8, 8, 0);
//LcdBarGraph  lbgb(&lcd, 8, 8, 1);

void serialdebuginfo()
{
  uint16_t    UvaRaw = VEML6075.readUvaRaw();         // read UVA raw
  uint16_t    UvbRaw = VEML6075.readUvbRaw();         // read UVB raw
  // compensation : measurement of visible noise
  uint16_t    comp1Raw = VEML6075.readUvComp1Raw();   // read COMP1 raw
  // compensation : measurement or IR noise
  uint16_t    comp2Raw = VEML6075.readUvComp2Raw();   // read COMP2 raw

  // integrated : getUva abd getUvb are already compensated.
  float       Uva = VEML6075.getUva();                // get UVA
  float       Uvb = VEML6075.getUvb();                // get UVB
  float       Uvi = VEML6075.getUvi(Uva, Uvb);        // get UV index

  Serial.println();
  Serial.println("======== start print ========");
  Serial.print("UVA raw:    ");
  Serial.println(UvaRaw);
  Serial.print("UVB raw:    ");
  Serial.println(UvbRaw);
  Serial.print("COMP1 raw:  ");
  Serial.println(comp1Raw);
  Serial.print("COMP2 raw:  ");
  Serial.println(comp2Raw);
  Serial.print("UVA:        ");
  Serial.println(Uva, 2);
  Serial.print("UVB:        ");
  Serial.println(Uvb, 2);
  Serial.print("UVIndex:    ");
  Serial.print(Uvi, 2);
  if(Uvi < UVI_LOW)
    Serial.println("  UVI low");
  else if(Uvi < UVI_MODERATE)
    Serial.println("  UVI moderate");
  else if(Uvi < UVI_HIGH)
    Serial.println("  UVI high");
  else if(Uvi < UVI_VERY_HIGH)
    Serial.println("  UVI very high");
  else
    Serial.println("  UVI extreme");
  Serial.print("mw/cm^2:    ");
  Serial.println(Uvi2mwpcm2(Uvi), 2);
  Serial.println("======== end print ========");
}

// blink the internal led, return the opposite state
// boolean blink(boolean state) {
//  digitalWrite(ledPin, state);
//  return !state;
//}
unsigned long blink(unsigned long thisTimeStamp, unsigned long blinkTimeStamp) {
  // TODO : we need a on/off setup for the bip
  if (thisTimeStamp - blinkTimeStamp > blinkInterval) {
    blinkTimeStamp = thisTimeStamp;
    // blink when dose > tgt
    // but do not blink if tgt=0
    if (((long)doseA / normUV) >= tgtA && tgtA != 0 ) {
      noTone(pinBip);
      tone(pinBip, bipHeight, bipDuration);
    } else {
      noTone(pinBip);    
    }
  }
  return blinkTimeStamp;
}

// do the acquisition of UVA and UVB, update dose
void acquisition() {
  // using compensated (for visible and IR noise) values
  float    Uva = VEML6075.getUva();
  float    Uvb = VEML6075.getUvb();
  Serial.print("Dose A + UvaRaw: ");
  Serial.print(doseA);
  Serial.print(" + ");
  Serial.println(Uva);
  Serial.print("Dose B + UVbRaw: ");
  Serial.print(doseB);
  Serial.print(" + ");
  Serial.println(Uvb);
//  doseA += (unsigned long)UvaRaw;
//  doseB += (unsigned long)UvbRaw;
  doseA += Uva;
  doseB += Uvb;
  if (mode == accumulate) {
    LCDacquisition(Uva, Uvb);
  }
}


void setup()
{
  // pin mode
 // pinMode(tgtPIN, INPUT);
  startingLCD();
  // init serial monitor
  Serial.begin(115200);
  // init UV sensor
  startingUVsensor();
  lcd.setCursor(0,2);
  lcd.print("VEML6075 boot OK");
  // start counting time
  startMeas = millis();
  // OneButton : attaching events
  // LCD on / off
  btn1.attachDoubleClick(doubleClickMode);
  // mode rotation
  btn1.attachClick(singleClickMode);
  // increase target in param mode
  btn2.attachClick(singleClickTgtplus);  
  // decrease target in param mode
  btn3.attachClick(singleClickTgtmoins);

  // to accelerate the increase or decrease we need to 
  // get a starting point (in time)
  btn2.attachPressStart(triggerTimer);
  btn3.attachPressStart(triggerTimer);
  // increase or decrease, with acceleration
  btn2.attachDuringLongPress(longPushTgtplus);
  btn3.attachDuringLongPress(longPushTgtmoins);

  // move to accumulate mode by default
  // starting with a small blocking delay
  delay(500);
  mode = accumulate;
  lcd.clear();
  LCDstaticAccumulate();
}

void loop() {
  // buttons tick
  btn1.tick();
  btn2.tick();
  btn3.tick();
  // polling or not polling
  unsigned long thisTimeStamp = millis();
  blinkTimeStamp = blink(thisTimeStamp, blinkTimeStamp);

  // do the acquisition
  if ((thisTimeStamp - measTimeStamp > measInterval)) {
    measTimeStamp = thisTimeStamp;
    acquisition();
    // uncomment next line to get detailed serial output
    //serialdebuginfo();
  }
 
  // are we in info mode ?
  LCDinfo(); 
}