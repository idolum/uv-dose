# UV dose

DIY UV dose measurement project for alternative photography.

## Purpose and feature

The purpose of this project is to build a UV dose measurement device, suitable for accurate exposure measurement of some so-called "alternative" photographic prints. Namely, this is to be used extensively for cyanotype prints.

The UV dose is measured in arbitrary units. The normalization to these arbitrary units is controlled by 2 parameters:

* the sampling period (default: every 2 seconds);
* a normalization factor (default : 3600);

These values have been calculated to provide roughly 200 arbitrary UV units when exposed 4 minutes on a bright May day light at noon.

Using this UV dose meter enables the photographer to have a repeatable process, using either an UV box (LED or bulbs), or using the Sun UVs with eventually some variations due to clouds absorption.

This devices has 2 ranges of measurements: UVA and UVB, respectively centered at 365nm and 330nm. See the Vishay application note in docs (designingveml6075.pdf) for more details. For cyanotype, the 365nm wavelength is the most interesting.

The UV dose meter will sample the UV illumination every 2 seconds, and accumulate the data to evaluate the cumulated insolation dose. The user can specify a target (in arbitrary units), the same for the UVA and UVB chanel. Once the dose reaches the target, a LED starts to blink, and the buzzer makes some noise.

Also provides the elapsed time since the beginning of the measurement.

## Usage

There are 3 buttons controlling the inputs, and a 16x2 LCD screen to display the information.

### Mode cycling

The button 1 cycle through the modes by single clicks:

* accumulate mode : display the live measurement (flux), the cumulated dose, and the target. One line per channel A, B.
* info mode: display the elapsed time since the last measurement start.

### Set UVA and UVB target

In accumulate mode, the button 2 and 3 are active:

* button 2 single click : increases the target by 1 unit
* button 3 single click : decreases the target by 1 unit
* button 2 long press: continuously increases the target
* button 3 long press: continuously decreases the target

### LCD on/off

The double click on button 1 toggles the LCD screen.
## Software

Using these libraries:

* for the UV sensor : DFRobot_VEML6075.h https://github.com/DFRobot/DFRobot_VEML6075
* wire https://www.arduino.cc/en/Reference/Wire
* LiquidCrystal : https://www.arduino.cc/en/Reference/LiquidCrystal
* OneButton https://github.com/mathertel/OneButton

## Hardware

Currently using:

* [VEML6075 UV sensor](https://www.dfrobot.com/product-1906.html) from DFRobot
* the [Arduino UNO](https://store.arduino.cc/arduino-uno-rev3) board
* 3 push buttons
* a 16x2 LCD display
* a few wires
* a piezoelectric buzzer

Actually I have been using the [Arduino Starter Kit](https://store.arduino.cc/genuino-starter-kit), with only the UV sensor as an additional sensor.

## Schematics

![SVG drawing of the ciruit schematics](./UVsensor/docs/schematics.svg)


## Improvements (aka TODO list)

* better integration (hardware) : no breadboard, packaging
* sensor connection : find some easy to use connector (jack ? )
* buzzer when the target is reached: done, but now we need some setup to inhibit the sound if don't want it (something else than the reset)
* LCD with integrated buttons ?
* battery level monitoring
* eventually control relays to switch of the UV box when the target is reached
